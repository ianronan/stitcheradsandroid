package wit.org.stitcheradsandroid;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.widget.TextView;

import com.facebook.CallbackManager;
import com.facebook.FacebookSdk;
import com.facebook.login.widget.ProfilePictureView;

/**
 * Created by ianie on 01/02/2016.
 */
public class ListActivity extends FragmentActivity {

    private TextView info;
    private ProfilePictureView profilePictureView;
    private CallbackManager callbackManager;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(getApplicationContext());

        info = (TextView) findViewById(R.id.info);
        callbackManager = CallbackManager.Factory.create();

        profilePictureView = (ProfilePictureView) findViewById(R.id.profilePicture);
        setContentView(R.layout.list_activity);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);

    }
}
